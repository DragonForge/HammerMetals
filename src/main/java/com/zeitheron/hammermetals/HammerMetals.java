package com.zeitheron.hammermetals;

import static com.zeitheron.hammermetals.api.HMLog.LOG;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.BiConsumer;

import org.apache.commons.io.FilenameUtils;

import com.zeitheron.hammercore.HammerCore;
import com.zeitheron.hammercore.intent.IntentManager;
import com.zeitheron.hammercore.internal.SimpleRegistration;
import com.zeitheron.hammercore.lib.zlib.error.JSONException;
import com.zeitheron.hammercore.lib.zlib.io.IOUtils;
import com.zeitheron.hammercore.lib.zlib.json.JSONArray;
import com.zeitheron.hammercore.lib.zlib.json.JSONObject;
import com.zeitheron.hammercore.lib.zlib.json.JSONTokener;
import com.zeitheron.hammercore.utils.ListUtils;
import com.zeitheron.hammermetals.api.GenSource;
import com.zeitheron.hammermetals.api.HMLog;
import com.zeitheron.hammermetals.api.MetalInfo;
import com.zeitheron.hammermetals.api.MetalRegistry;
import com.zeitheron.hammermetals.api.WorldGenInfo;
import com.zeitheron.hammermetals.api.events.PlayHammerSoundEvent;
import com.zeitheron.hammermetals.api.parts.EnumBlockMetalPart;
import com.zeitheron.hammermetals.api.parts.EnumItemMetalPart;
import com.zeitheron.hammermetals.cfg.ConfigsHM;
import com.zeitheron.hammermetals.init.BlocksHM;
import com.zeitheron.hammermetals.init.ItemsHM;
import com.zeitheron.hammermetals.proxy.CommonProxy;

import net.minecraft.crash.CrashReport;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ReportedException;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.Tuple;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.crafting.CraftingHelper;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.ModContainer;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLConstructionEvent;
import net.minecraftforge.fml.common.event.FMLFingerprintViolationEvent;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import net.minecraftforge.registries.IForgeRegistryEntry;

@Mod(modid = InfoHM.MOD_ID, name = InfoHM.MOD_NAME, version = InfoHM.MOD_VERSION, dependencies = "required-after:hammercore", certificateFingerprint = "4d7b29cd19124e986da685107d16ce4b49bc0a97", guiFactory = "com.zeitheron.hammermetals.cfg.ConfigFactoryHM")
public class HammerMetals
{
	@Instance
	public static HammerMetals instance;
	
	@SidedProxy(serverSide = "com.zeitheron.hammermetals.proxy.CommonProxy", clientSide = "com.zeitheron.hammermetals.proxy.ClientProxy")
	public static CommonProxy proxy;
	
	private static long recipeId = 0L;
	
	public static File customMetalsDir = new File("config", InfoHM.MOD_ID + File.separator + "custom_metals");
	
	public static final CreativeTabs TAB = new CreativeTabs(InfoHM.MOD_ID)
	{
		@Override
		public ItemStack getTabIconItem()
		{
			return new ItemStack(MetalRegistry.getMetal("platinum").get(EnumItemMetalPart.GEAR));
		}
		
		@Override
		public void displayAllRelevantItems(NonNullList<ItemStack> items)
		{
			NonNullList<ItemStack> nnl = NonNullList.create();
			super.displayAllRelevantItems(nnl);
			nnl.sort((a, b) -> a.getDisplayName().compareTo(b.getDisplayName()));
			items.addAll(nnl);
		}
	};
	
	@EventHandler
	public void certificateViolation(FMLFingerprintViolationEvent e)
	{
		LOG.warn("*****************************");
		LOG.warn("WARNING: Somebody has been tampering with HammerMetals jar!");
		LOG.warn("It is highly recommended that you redownload mod from https://minecraft.curseforge.com/projects/279905 !");
		LOG.warn("*****************************");
		HammerCore.invalidCertificates.put(InfoHM.MOD_ID, "https://minecraft.curseforge.com/projects/279905");
	}
	
	@EventHandler
	public void construct(FMLConstructionEvent e)
	{
		IntentManager.registerIntentHandler(InfoHM.MOD_ID + ":get_metal", String.class, (mod, data) -> MetalRegistry.getMetal(data));
		IntentManager.registerIntentHandler(InfoHM.MOD_ID + ":register_metal_builder", Tuple.class, (mod, data) -> MetalRegistry.newBuilder((String) data.getFirst(), mod, (Boolean) data.getSecond()));
		IntentManager.registerIntentHandler(InfoHM.MOD_ID + ":register_metal_json", String.class, (mod, data) ->
		{
			JSONObject obj = null;
			
			try
			{
				obj = (JSONObject) new JSONTokener(data).nextValue();
			} catch(JSONException e1)
			{
				HMLog.LOG.warn("Unable to parse JSON: %s", data);
				e1.printStackTrace();
				return null;
			}
			
			final JSONObject entry = obj;
			
			MetalInfo info = MetalRegistry.newBuilder(obj.optString("id"), obj.optString("mod", mod), obj.has("alloy")).build();
			info.useTextureTemplate = obj.has("color");
			info.useOreTemplate = obj.optBoolean("useOreTemplate");
			JSONObject jobj = obj.optJSONObject("langs");
			if(jobj != null)
				try
				{
					info.useGeneratedLang = true;
					info.langs.deserializeJson(jobj);
				} catch(Throwable err)
				{
					err.printStackTrace();
				}
			info.color = info.useTextureTemplate ? obj.optInt("color") : 0xFFFFFF;
			
			JSONArray arr = obj.optJSONArray("addItems");
			if(arr != null)
				for(Object v : arr.values())
				{
					String name = (v + "").toUpperCase();
					if(name.equalsIgnoreCase("all"))
						for(EnumItemMetalPart part : EnumItemMetalPart.values())
							info.itemParts.add(part);
					else if(name.startsWith("!"))
						info.itemParts.remove(EnumItemMetalPart.valueOf(name.substring(1)));
					else
					{
						EnumItemMetalPart part = EnumItemMetalPart.valueOf(name);
						if(part != null)
							info.itemParts.add(part);
					}
				}
			
			arr = obj.optJSONArray("addBlocks");
			if(arr != null)
				for(Object v : arr.values())
				{
					String name = (v + "").toUpperCase();
					if(name.equalsIgnoreCase("all"))
						for(EnumBlockMetalPart part : EnumBlockMetalPart.values())
							info.blockParts.add(part);
					else if(name.startsWith("!"))
						info.blockParts.remove(EnumBlockMetalPart.valueOf(name.substring(1)));
					else
					{
						EnumBlockMetalPart part = EnumBlockMetalPart.valueOf(name);
						if(part != null)
							info.blockParts.add(part);
					}
				}
			
			info.maxInChunk = obj.optInt("maxInChunk", 0);
			info.minY = obj.optInt("minY", 0);
			info.maxY = obj.optInt("maxY", 32);
			
			if(info.alloy != null)
			{
				JSONObject al = obj.optJSONObject("alloy");
				info.alloy.output = al.optInt("output", 1);
				info.alloy.metals = new ArrayList<>();
				arr = al.optJSONArray("inputs");
				for(int i = 0; i < arr.length(); ++i)
					info.alloy.metals.add(arr.optString(i));
			}
			
			info.maxVeinSize = obj.optInt("maxVeinSize", 0);
			try
			{
				JSONObject o = obj.optJSONObject("gen");
				
				/** Parse extra world-gen data */
				if(o != null)
				{
					WorldGenInfo inf = info.gen;
					
					JSONObject src = o.optJSONObject("gen_source");
					if(src != null)
						inf.gen_src = new GenSource(src.getString("block"), src.optInt("meta", 0));
					
					inf.biomeWhitelist = o.optBoolean("biomeWhitelist", false);
					inf.dimensionWhitelist = o.optBoolean("dimensionWhitelist", false);
					
					JSONArray bios = o.optJSONArray("biomes");
					if(bios != null && bios.length() > 0)
						for(int i = 0; i < bios.length(); ++i)
							inf.biomes.add(bios.getString(i));
						
					JSONArray dims = o.optJSONArray("dimensions");
					if(dims != null && dims.length() > 0)
						for(int i = 0; i < dims.length(); ++i)
							inf.dimensions.add(dims.getInt(i));
				}
			} catch(JSONException e1)
			{
				e1.printStackTrace();
			}
			MetalRegistry.register(info);
			
			return info;
		});
		
		if(!customMetalsDir.isDirectory())
		{
			customMetalsDir.mkdirs();
			
			HMLog.LOG.info("Custom metal directory not found. Creating a new one for you...");
			
			try(InputStream from = HammerMetals.class.getResourceAsStream("/assets/hammermetals/hmetals/_example.json"))
			{
				FileOutputStream out = new FileOutputStream(new File(customMetalsDir, "_example.json"));
				out.write(IOUtils.pipeOut(from));
				out.close();
			} catch(Throwable err)
			{
			}
		}
		
		for(ModContainer mod : Loader.instance().getActiveModList())
		{
			List<String> mods = new ArrayList<>();
			if(forEachHMetalFile(mod, (name, json) ->
			{
				if(MetalRegistry.metalSet().contains(name))
				{
					LOG.info("Skipping already indexed metal " + name + ".");
					return;
				}
				
				LOG.info("Parsing /" + mod.getModId() + "/hmetals/" + name + ".json ...");
				
				IntentManager.sendIntent(InfoHM.MOD_ID + ":register_metal_json", json.toString());
				
				if(!mods.contains(mod.getModId()))
					mods.add(mod.getModId());
			}))
				if(mods.contains(mod.getModId()))
					LOG.info("Added metal indices for mod " + mod.getModId());
				else
					;
			else
				LOG.info("Could not add metals for mod " + mod.getModId());
		}
		
		HMLog.LOG.info("Loading custom metals directory...");
		for(File f : customMetalsDir.listFiles(file -> file.isFile() && file.getName().endsWith(".json") && !file.getName().startsWith("_")))
			try(BufferedReader br = Files.newBufferedReader(f.toPath()))
			{
				StringBuilder data = new StringBuilder();
				br.lines().forEach(data::append);
				JSONObject jo = (JSONObject) new JSONTokener(data.toString()).nextValue();
				jo.put("id", f.getName().substring(0, f.getName().length() - 5));
				IntentManager.sendIntent(InfoHM.MOD_ID + ":register_metal_json", jo.toString());
			} catch(Throwable err)
			{
				throw new ReportedException(CrashReport.makeCrashReport(err, "Exception caught while loading metals from json " + f.getAbsolutePath()));
			}
	}
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent e)
	{
		MinecraftForge.EVENT_BUS.register(instance);
		MinecraftForge.EVENT_BUS.register(proxy);
		if(ConfigsHM.master_oreUnification)
			MinecraftForge.EVENT_BUS.register(new OreUnification());
		
		MetalRegistry.preInit(metal -> ConfigsHM.config.getBoolean(metal.idUpper() + " Enabled", "Metals", true, "Should " + metal.idUpper() + " be enabled? WARNING: This only disables recipes, world gen and hides items and blocks from creative view! This is done to prevent item loss. If ore unification is enabled, these materials will try to convert to other types."));
		if(ConfigsHM.config.hasChanged())
			ConfigsHM.config.save();
		
		SimpleRegistration.registerFieldBlocksFrom(BlocksHM.class, InfoHM.MOD_ID, TAB);
		SimpleRegistration.registerFieldItemsFrom(ItemsHM.class, InfoHM.MOD_ID, TAB);
		
		// if(MetalRegistry.hasMetal("gold"))
		// {
		// if(MetalRegistry.getMetal("gold").has(EnumItemMetalPart.DUST))
		// GameRegistry.addSmelting(MetalRegistry.getMetal("gold").get(EnumItemMetalPart.DUST),
		// new ItemStack(Items.GOLD_INGOT), 0F);
		//
		// if(MetalRegistry.getMetal("gold").has(EnumItemMetalPart.PLATE))
		// GameRegistry.addSmelting(MetalRegistry.getMetal("gold").get(EnumItemMetalPart.PLATE),
		// new ItemStack(Items.GOLD_INGOT), 0F);
		//
		// if(MetalRegistry.getMetal("gold").has(EnumItemMetalPart.GEAR))
		// GameRegistry.addSmelting(new
		// ItemStack(MetalRegistry.getMetal("gold").get(EnumItemMetalPart.GEAR)),
		// new ItemStack(Items.GOLD_INGOT, 4), 1F);
		// }
		//
		// if(MetalRegistry.hasMetal("iron"))
		// {
		// if(MetalRegistry.getMetal("iron").has(EnumItemMetalPart.DUST))
		// GameRegistry.addSmelting(MetalRegistry.getMetal("iron").get(EnumItemMetalPart.DUST),
		// new ItemStack(Items.IRON_INGOT), 0F);
		//
		// if(MetalRegistry.getMetal("iron").has(EnumItemMetalPart.PLATE))
		// GameRegistry.addSmelting(MetalRegistry.getMetal("iron").get(EnumItemMetalPart.PLATE),
		// new ItemStack(Items.IRON_INGOT), 0F);
		// }
	}
	
	public static boolean isFromHM(IForgeRegistryEntry entry)
	{
		return entry.getRegistryName() != null && entry.getRegistryName().getResourceDomain().equals(InfoHM.MOD_ID);
	}
	
	@EventHandler
	public void init(FMLInitializationEvent e)
	{
		proxy.init();
	}
	
	@SubscribeEvent
	public void onCraft(PlayerEvent.ItemCraftedEvent evt)
	{
		if(!evt.player.world.isRemote)
		{
			IInventory inv = evt.craftMatrix;
			boolean play = false;
			for(int i = 0; i < inv.getSizeInventory(); ++i)
			{
				if(inv.getStackInSlot(i).getItem() != ItemsHM.HAMMER)
					continue;
				play = true;
			}
			if(play)
			{
				PlayHammerSoundEvent phse = new PlayHammerSoundEvent(evt.player, evt.crafting, evt.craftMatrix);
				MinecraftForge.EVENT_BUS.post(phse);
				HammerCore.audioProxy.playSoundAt(evt.player.world, phse.getPlaySound(), evt.player.getPosition(), .05F, .7F, SoundCategory.PLAYERS);
			}
		}
	}
	
	public static boolean forEachHMetalFile(ModContainer mod, BiConsumer<String, JSONObject> json)
	{
		return CraftingHelper.findFiles(mod, "assets/" + mod.getModId() + "/hmetals", root -> true, (root, file) ->
		{
			Loader.instance().setActiveModContainer(mod);
			
			String relative = root.relativize(file).toString();
			if(!"json".equals(FilenameUtils.getExtension(file.toString())) || relative.startsWith("_"))
				return true;
			
			String name = FilenameUtils.removeExtension(relative).replaceAll("\\\\", "/");
			ResourceLocation key = new ResourceLocation(mod.getModId(), name);
			
			try(BufferedReader br = Files.newBufferedReader(file))
			{
				StringBuilder data = new StringBuilder();
				br.lines().forEach(data::append);
				JSONObject jo = (JSONObject) new JSONTokener(data.toString()).nextValue();
				
				if(jo.has("id"))
				{
					String author = ListUtils.random(mod.getMetadata().authorList, new Random());
					if(author == null)
						author = mod.getName() + (mod.getName().toLowerCase().endsWith("'") ? "" : mod.getName().toLowerCase().endsWith("s") ? "'" : "'s") + " Author";
					LOG.warn("WARNING, " + author + "! Remove \"id\" tag from /assets/" + mod.getModId() + "/hmetals/" + name + ".json");
				}
				
				jo.put("mod", mod.getModId());
				jo.put("id", name);
				json.accept(name, jo);
			} catch(Throwable err)
			{
				throw new ReportedException(CrashReport.makeCrashReport(err, "Exception caught while loading metals from json"));
			}
			
			return true;
		}, true, true);
	}
}