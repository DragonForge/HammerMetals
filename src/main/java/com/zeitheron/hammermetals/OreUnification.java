package com.zeitheron.hammermetals;

import java.util.ArrayList;
import java.util.List;

import com.zeitheron.hammercore.utils.InterItemStack;
import com.zeitheron.hammermetals.api.HMLog;
import com.zeitheron.hammermetals.api.MetalData;
import com.zeitheron.hammermetals.api.MetalRegistry;
import com.zeitheron.hammermetals.api.parts.EnumBlockMetalPart;
import com.zeitheron.hammermetals.api.parts.EnumItemMetalPart;

import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import net.minecraftforge.event.entity.player.EntityItemPickupEvent;
import net.minecraftforge.event.world.BlockEvent.HarvestDropsEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.ItemPickupEvent;
import net.minecraftforge.oredict.OreDictionary;

public class OreUnification
{
	public static final List<String> WHITELIST_ORE_SUBS = new ArrayList<>();
	
	{
		HMLog.LOG.warn("*****************************");
		HMLog.LOG.warn("WARNING: Enabled OreUnification!");
		HMLog.LOG.warn("This feature is under development, but it in fact functions correctly.");
		HMLog.LOG.warn("*****************************");
	}
	
	static
	{
		for(EnumItemMetalPart part : EnumItemMetalPart.values())
			WHITELIST_ORE_SUBS.add(part.od);
		for(EnumBlockMetalPart part : EnumBlockMetalPart.values())
			WHITELIST_ORE_SUBS.add(part.od);
	}
	
	private static ItemStack convert(ItemStack stack, String ore)
	{
		if(OreDictionary.getOres(ore).isEmpty())
			return stack.copy();
		NonNullList<ItemStack> possible = OreDictionary.getOres(ore);
		ItemStack ns = ItemStack.EMPTY;
		
		String metal = ore;
		for(String wh : WHITELIST_ORE_SUBS)
			if(ore.startsWith(wh))
			{
				metal = ore.substring(wh.length());
				break;
			}
		
		MetalData data = MetalRegistry.getMetal(metal);
		boolean disabled = data != null && !data.enable.getAsBoolean();
		
		// Preffer Hammer Metals' items.
		for(ItemStack d : possible)
			if(!disabled && d.getItem().getRegistryName().getResourceDomain().equals(InfoHM.MOD_ID))
			{
				ns = d.copy();
				break;
			} else if(disabled && !d.getItem().getRegistryName().getResourceDomain().equals(InfoHM.MOD_ID))
			{
				ns = d.copy();
				break;
			}
		
		// No ores can be found
		if(ns.isEmpty())
			return stack.copy();
		
		if(ns.getItemDamage() == OreDictionary.WILDCARD_VALUE)
			ns.setItemDamage(0);
		
		ns.setCount(ns.getCount() * stack.getCount());
		ns.setTagCompound(stack.hasTagCompound() ? stack.getTagCompound().copy() : null);
		return ns;
	}
	
	public static ItemStack tryUnify(ItemStack stack)
	{
		if(InterItemStack.isStackNull(stack))
			return stack;
		for(int id : OreDictionary.getOreIDs(stack))
		{
			String name = OreDictionary.getOreName(id);
			for(String wh : WHITELIST_ORE_SUBS)
				if(name.startsWith(wh))
					return convert(stack, name);
		}
		return stack;
	}
	
	public static EntityItem tryUnify(EntityItem ei)
	{
		if(ei == null || InterItemStack.isStackNull(ei.getItem()) || OreDictionary.getOreIDs(ei.getItem()).length == 0)
			return ei;
		ei.setItem(tryUnify(ei.getItem()));
		return ei;
	}
	
	@SubscribeEvent
	public void pickEntityItem(EntityItemPickupEvent e)
	{
		tryUnify(e.getItem());
	}
	
	@SubscribeEvent
	public void pickupItem(ItemPickupEvent e)
	{
		tryUnify(e.pickedUp);
	}
	
	@SubscribeEvent
	public void drops(HarvestDropsEvent e)
	{
		List<ItemStack> stacks = e.getDrops();
		for(int i = 0; i < stacks.size(); ++i)
			stacks.set(i, tryUnify(stacks.get(i)));
	}
	
	@SubscribeEvent
	public void drops(LivingDropsEvent e)
	{
		List<EntityItem> stacks = e.getDrops();
		for(int i = 0; i < stacks.size(); ++i)
			stacks.set(i, tryUnify(stacks.get(i)));
	}
}