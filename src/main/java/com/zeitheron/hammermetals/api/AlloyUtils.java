package com.zeitheron.hammermetals.api;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

import com.zeitheron.hammermetals.api.parts.EnumItemMetalPart;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.oredict.OreDictionary;

public class AlloyUtils
{
	public static class Alloy
	{
		public final MetalInfo metal;
		public final AlloyInfo alloy;
		
		public Alloy(MetalInfo mi)
		{
			this.metal = mi;
			this.alloy = mi.alloy;
		}
	}
	
	public static MetalData getMetalFromStack(ItemStack item)
	{
		if(item.isEmpty())
			return null;
		
		String ingot = null;
		for(int i : OreDictionary.getOreIDs(item))
		{
			String na = OreDictionary.getOreName(i);
			if(na.startsWith("ingot"))
			{
				ingot = na.substring(5);
				break;
			}
		}
		
		if(ingot != null)
			return MetalRegistry.getMetal(ingot);
		
		return null;
	}
	
	public static List<Alloy> listRegisteredAlloys()
	{
		return new ArrayList<>(MetalRegistry.metalSet().stream().map(MetalRegistry::getMetal).map(d -> d.info).map(Alloy::new).filter(a -> a.alloy != null).collect(Collectors.toList()));
	}
	
	public static boolean canCraft(AlloyInfo info, List<String> metals)
	{
		List<String> removed = new ArrayList<>();
		for(String met : info.metals)
			if(metals.contains(met))
			{
				removed.add(met);
				metals.remove(met);
			}
		if(info.metals.size() == removed.size())
			return true;
		metals.addAll(removed);
		return false;
	}
	
	public static List<String> getCraftableAlloys(List<String> metals)
	{
		metals = new ArrayList<>(metals);
		List<String> alloys = new ArrayList<>();
		List<Alloy> registered = listRegisteredAlloys();
		
		while(true)
		{
			boolean crafted = false;
			for(Alloy a : registered)
				if(canCraft(a.alloy, metals))
				{
					metals.removeAll(a.alloy.metals);
					alloys.add(a.metal.id);
					crafted = true;
					break;
				}
			if(!crafted)
				break;
		}
		
		return alloys;
	}
	
	public static int getAlloys(List<String> alloys, String alloy)
	{
		return alloys.stream().filter(alloy::equals).collect(Collectors.toList()).size();
	}
	
	public static Ingredient alloyInputToIngredient(String input, @Nullable EnumItemMetalPart part)
	{
		if(input.startsWith("item."))
		{
			int len = input.contains("@") ? input.lastIndexOf("@") : input.length();
			int meta = input.contains("@") ? Integer.parseInt(input.substring(input.lastIndexOf("@") + 1)) : -1;
			Item item = ForgeRegistries.ITEMS.getValue(new ResourceLocation(input.substring(6, len)));
			if(meta == -1)
				return Ingredient.fromItem(item);
			else
				return Ingredient.fromStacks(new ItemStack(item, 1, meta));
		} else if(input.startsWith("ore."))
			return Ingredient.fromStacks(OreDictionary.getOres(input.substring(4)).toArray(new ItemStack[0]));
		if(part == null)
			return Ingredient.EMPTY;
		MetalData md = MetalRegistry.getMetal(input);
		if(!md.has(part))
			return Ingredient.EMPTY;
		return Ingredient.fromItem(md.get(part));
	}
}