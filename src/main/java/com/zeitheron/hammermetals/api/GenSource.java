package com.zeitheron.hammermetals.api;

import com.zeitheron.hammercore.lib.zlib.json.serapi.Jsonable;
import com.zeitheron.hammercore.lib.zlib.json.serapi.SerializedName;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class GenSource implements Jsonable
{
	@SerializedName("meta")
	public int metadata;
	
	public String block;
	
	public GenSource()
	{
	}
	
	public GenSource(String block, int meta)
	{
		this.metadata = meta;
		this.block = block;
	}
	
	public IBlockState asState()
	{
		if(this.block == null || this.block.isEmpty())
			return null;
		Block block = GameRegistry.findRegistry(Block.class).getValue(new ResourceLocation(this.block));
		if(block == null)
			return null;
		return block.getStateFromMeta(metadata);
	}
}