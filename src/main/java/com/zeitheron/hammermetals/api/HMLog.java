package com.zeitheron.hammermetals.api;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.zeitheron.hammermetals.InfoHM;

public class HMLog
{
	public static final Logger LOG = LogManager.getLogger(InfoHM.MOD_ID);
}