package com.zeitheron.hammermetals.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;

import javax.annotation.Nullable;

import net.minecraftforge.fml.common.ProgressManager;
import net.minecraftforge.fml.common.ProgressManager.ProgressBar;
import net.minecraftforge.fml.common.event.FMLConstructionEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

/**
 * Everything MUST be registered before {@link FMLPreInitializationEvent}. Use
 * {@link FMLConstructionEvent}.
 */
public class MetalRegistry
{
	private static Map<String, MetalInfo> metals = new HashMap<>();
	private static Map<String, MetalData> mdatas = new HashMap<>();
	
	public static MetalBuilder newBuilder(String metalId, String mod, boolean alloy)
	{
		return new MetalBuilder(metalId.toLowerCase(), mod.toLowerCase(), alloy);
	}
	
	public static void register(MetalInfo info)
	{
		metals.put(info.id.toLowerCase(), info);
	}
	
	public static Set<String> metalSet()
	{
		return metals.keySet();
	}
	
	@Nullable
	public static MetalData getMetal(String id)
	{
		return mdatas.get(id.toLowerCase());
	}
	
	public static boolean hasMetal(String id)
	{
		return mdatas.containsKey(id.toLowerCase());
	}
	
	public static void preInit(Predicate<MetalInfo> enable)
	{
		if(!mdatas.isEmpty())
			return;
		
		HMLog.LOG.info("Loading metals...");
		ProgressBar bar = ProgressManager.push("Adding Metals...", metals.size(), true);
		List<String> l = new ArrayList<>(metals.keySet());
		l.sort((a, b) -> a.compareTo(b));
		l.stream().map(metals::get).forEach(i ->
		{
			bar.step(Character.toUpperCase(i.id.charAt(0)) + i.id.substring(1));
			mdatas.put(i.id.toLowerCase(), new MetalData(i, enable));
		});
		ProgressManager.pop(bar);
		HMLog.LOG.info("Metals Registered.");
	}
}