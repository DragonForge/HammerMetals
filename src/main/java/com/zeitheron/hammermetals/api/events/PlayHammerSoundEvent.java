package com.zeitheron.hammermetals.api.events;

import com.zeitheron.hammercore.utils.ItemStackUtil;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.oredict.OreDictionary;

public class PlayHammerSoundEvent extends PlayerEvent
{
	public String playSound = "block.anvil.use";
	public final IInventory craftMatrix;
	public final ItemStack output;
	
	public PlayHammerSoundEvent(EntityPlayer player, ItemStack crafting, IInventory craftMatrix)
	{
		super(player);
		this.output = crafting;
		this.craftMatrix = craftMatrix;
	}
	
	public int getGridItems(Block ofType)
	{
		if(Item.getItemFromBlock(ofType) == null)
		{
			return 0;
		}
		return this.getGridItems(Item.getItemFromBlock(ofType));
	}
	
	public int getGridItems(Item ofType)
	{
		int o = 0;
		for(int i = 0; i < this.craftMatrix.getSizeInventory(); ++i)
		{
			if(this.craftMatrix.getStackInSlot(i).getItem() != ofType)
				continue;
			o += this.craftMatrix.getStackInSlot(i).getCount();
		}
		return o;
	}
	
	public int getGridItems(String ofType)
	{
		int o = 0;
		for(int i = 0; i < this.craftMatrix.getSizeInventory(); ++i)
		{
			if(!PlayHammerSoundEvent.odMatches(ofType, this.craftMatrix.getStackInSlot(i)))
				continue;
			o += this.craftMatrix.getStackInSlot(i).getCount();
		}
		return o;
	}
	
	private static boolean odMatches(String od, ItemStack item)
	{
		for(int id : OreDictionary.getOreIDs(item))
		{
			if(!OreDictionary.getOreName(id).equals(od))
				continue;
			return true;
		}
		return false;
	}
	
	public int getGridItems(ItemStack ofType)
	{
		int o = 0;
		for(int i = 0; i < this.craftMatrix.getSizeInventory(); ++i)
		{
			if(!ItemStackUtil.itemsEqual(this.craftMatrix.getStackInSlot(i), ofType))
				continue;
			o += this.craftMatrix.getStackInSlot(i).getCount();
		}
		return o;
	}
	
	public ItemStack getCraftResult()
	{
		return this.output;
	}
	
	public PlayHammerSoundEvent setPlaySound(String newSound)
	{
		this.playSound = newSound;
		return this;
	}
	
	public String getPlaySound()
	{
		return this.playSound;
	}
}