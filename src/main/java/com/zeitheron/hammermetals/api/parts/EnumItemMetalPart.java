package com.zeitheron.hammermetals.api.parts;

import java.util.Arrays;

public enum EnumItemMetalPart
{
	INGOT("ingot", 1), //
	DUST("dust", 1), //
	GEAR("gear", 4), //
	PLATE("plate", 2), //
	NUGGET("nugget", 1 / 9F), //
	ROD("stick", 1), //
	COIL("coil", 1.5F);
	
	public final String sub;
	public final String od;
	
	public float ingotsWorth;
	
	private EnumItemMetalPart(String od, float ingotsWorth)
	{
		this.sub = name().toLowerCase();
		this.ingotsWorth = ingotsWorth;
		this.od = od;
	}
	
	public float getIngotsWorth()
	{
		return ingotsWorth;
	}
	
	public static EnumItemMetalPart[] sortByCost(boolean reverse)
	{
		EnumItemMetalPart[] parts = values().clone();
		Arrays.sort(parts, (a, b) -> a.ingotsWorth > b.ingotsWorth ? 1 : a.ingotsWorth < b.ingotsWorth ? -1 : 0);
		return parts;
	}
}