package com.zeitheron.hammermetals.items;

import java.util.function.BooleanSupplier;
import java.util.function.Supplier;

import com.zeitheron.hammercore.internal.init.ItemsHC;
import com.zeitheron.hammercore.utils.IRegisterListener;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.text.translation.I18n;

public class ItemParametered<T> extends Item implements IRegisterListener
{
	public T t;
	public BooleanSupplier enable = () -> true;
	public boolean registerModel = true;
	
	public String sub;
	public Supplier<String> material = () -> "Unknown";
	
	public T getParameter()
	{
		return t;
	}
	
	public ItemParametered<T> setParameter(T t)
	{
		this.t = t;
		return this;
	}
	
	public ItemParametered<T> setEnable(BooleanSupplier enable)
	{
		this.enable = enable;
		return this;
	}
	
	@Override
	public String getItemStackDisplayName(ItemStack stack)
	{
		if(sub != null)
			return I18n.translateToLocalFormatted(sub, material.get());
		else
			return super.getItemStackDisplayName(stack);
	}
	
	@Override
	public void onRegistered()
	{
		if(!registerModel)
			ItemsHC.items.remove(this);
	}
	
	@Override
	public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> items)
	{
		if(enable.getAsBoolean())
			super.getSubItems(tab, items);
	}
}