package com.zeitheron.hammermetals.jei.alloy;

import java.util.stream.Collectors;

import com.zeitheron.hammermetals.api.AlloyInfo;
import com.zeitheron.hammermetals.api.MetalData;
import com.zeitheron.hammermetals.api.MetalRegistry;
import com.zeitheron.hammermetals.api.parts.EnumBlockMetalPart;
import com.zeitheron.hammermetals.api.parts.EnumItemMetalPart;
import com.zeitheron.hammermetals.collect.HMCollectors;

import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;

public class AlloyRecipe
{
	public final AlloyInfo info;
	public final MetalData data;
	
	public EnumItemMetalPart itemPart;
	public EnumBlockMetalPart blockPart;
	
	public static AlloyRecipe create(MetalData data)
	{
		return new AlloyRecipe(data);
	}
	
	public AlloyRecipe(MetalData data)
	{
		this.data = data;
		this.info = data.info.alloy;
		
		for(EnumItemMetalPart ip : EnumItemMetalPart.sortByCost(false))
			if(allHave(ip))
			{
				itemPart = ip;
				break;
			}
		
		if(itemPart == null)
			for(EnumBlockMetalPart bp : EnumBlockMetalPart.sortByCost(false))
				if(allHave(bp))
				{
					blockPart = bp;
					break;
				}
	}
	
	private boolean allHave(EnumItemMetalPart p)
	{
		return !info.metals.stream().map(MetalRegistry::getMetal).map(metal -> metal.has(p)).collect(Collectors.toList()).contains(false);
	}
	
	private boolean allHave(EnumBlockMetalPart p)
	{
		return !info.metals.stream().map(MetalRegistry::getMetal).map(metal -> metal.has(p)).collect(Collectors.toList()).contains(false);
	}
	
	public ItemStack getPart(MetalData md)
	{
		return blockPart != null ? new ItemStack(md.get(blockPart)) : new ItemStack(md.get(itemPart));
	}
	
	public NonNullList<ItemStack> getInputs()
	{
		return info.metals.stream().map(MetalRegistry::getMetal).map(this::getPart).collect(HMCollectors.toNNList());
	}
	
	public ItemStack getOutput()
	{
		ItemStack is = getPart(data);
		is.setCount(info.output);
		return is;
	}
}