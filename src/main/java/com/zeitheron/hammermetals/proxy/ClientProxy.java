package com.zeitheron.hammermetals.proxy;

import java.io.BufferedReader;
import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zeitheron.hammercore.lib.zlib.json.JSONObject;
import com.zeitheron.hammercore.lib.zlib.json.JSONTokener;
import com.zeitheron.hammermetals.HammerMetals;
import com.zeitheron.hammermetals.api.HMLog;
import com.zeitheron.hammermetals.api.MetalData;
import com.zeitheron.hammermetals.api.MetalInfo;
import com.zeitheron.hammermetals.api.MetalRegistry;
import com.zeitheron.hammermetals.api.events.ReloadMetalColorsEvent;
import com.zeitheron.hammermetals.api.parts.EnumBlockMetalPart;

import it.unimi.dsi.fastutil.objects.Object2IntMap;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.crash.CrashReport;
import net.minecraft.item.Item;
import net.minecraft.util.ReportedException;
import net.minecraftforge.client.event.ModelBakeEvent;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.ModContainer;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class ClientProxy extends CommonProxy
{
	@Override
	public void init()
	{
		Minecraft mc = Minecraft.getMinecraft();
		final List<Item> ils = new ArrayList<>();
		
		MetalRegistry.metalSet().stream().map(MetalRegistry::getMetal).forEach(data ->
		{
			boolean tex = data.info.useTextureTemplate;
			
			data.getItems().stream().filter(HammerMetals::isFromHM).forEach(ils::add);
			mc.getItemColors().registerItemColorHandler((stack, tint) -> tint == 0 ? data.info.color : 0xFFFFFF, ils.toArray(new Item[ils.size()]));
			ils.clear();
			
			if(data.has(EnumBlockMetalPart.ORE) && HammerMetals.isFromHM(data.get(EnumBlockMetalPart.ORE)) && data.info.useOreTemplate)
				mc.getBlockColors().registerBlockColorHandler((state, world, pos, tint) -> tint == 0 ? data.info.color : 0xFFFFFF, data.get(EnumBlockMetalPart.ORE));
			
			List<Block> blocks = data.getBlocks();
			blocks.remove(data.get(EnumBlockMetalPart.ORE));
			blocks.removeIf(b -> !HammerMetals.isFromHM(b));
			if(tex)
				mc.getBlockColors().registerBlockColorHandler((state, world, pos, tint) -> data.info.color, blocks.toArray(new Block[blocks.size()]));
			
			data.registerModels();
			
			if(data.info.useTextureTemplate)
				for(EnumBlockMetalPart part : EnumBlockMetalPart.values())
					if(part.notOre() && data.has(part) && HammerMetals.isFromHM(data.get(part)))
						Minecraft.getMinecraft().getBlockRendererDispatcher().getBlockModelShapes().registerBlockWithStateMapper(data.get(part), block ->
						{
							Map<IBlockState, ModelResourceLocation> models = new HashMap<>();
							ModelResourceLocation mrl = new ModelResourceLocation("hammermetals:tin_" + part.sub, "normal");
							for(IBlockState state : block.getBlockState().getValidStates())
								models.put(state, mrl);
							return models;
						});
					else if(!part.notOre() && data.has(part) && HammerMetals.isFromHM(data.get(part)) && data.info.useOreTemplate)
						Minecraft.getMinecraft().getBlockRendererDispatcher().getBlockModelShapes().registerBlockWithStateMapper(data.get(part), block ->
						{
							Map<IBlockState, ModelResourceLocation> models = new HashMap<>();
							ModelResourceLocation mrl = new ModelResourceLocation("hammermetals:template_ore", "normal");
							for(IBlockState state : block.getBlockState().getValidStates())
								models.put(state, mrl);
							return models;
						});
		});
	}
	
	@SubscribeEvent
	public void modelBake(ModelBakeEvent e)
	{
		HMLog.LOG.info("Reloading custom metal blocks and ores...");
		MetalRegistry.metalSet().stream().map(MetalRegistry::getMetal).forEach(data ->
		{
			if(data.info.useTextureTemplate)
				for(EnumBlockMetalPart part : EnumBlockMetalPart.values())
					if(part.notOre() && data.has(part) && HammerMetals.isFromHM(data.get(part)))
						e.getModelManager().getBlockModelShapes().registerBlockWithStateMapper(data.get(part), block ->
						{
							Map<IBlockState, ModelResourceLocation> models = new HashMap<>();
							ModelResourceLocation mrl = new ModelResourceLocation("hammermetals:tin_" + part.sub, "normal");
							for(IBlockState state : block.getBlockState().getValidStates())
								models.put(state, mrl);
							return models;
						});
					else if(!part.notOre() && data.has(part) && HammerMetals.isFromHM(data.get(part)) && data.info.useOreTemplate)
						e.getModelManager().getBlockModelShapes().registerBlockWithStateMapper(data.get(part), block ->
						{
							Map<IBlockState, ModelResourceLocation> models = new HashMap<>();
							ModelResourceLocation mrl = new ModelResourceLocation("hammermetals:template_ore", "normal");
							for(IBlockState state : block.getBlockState().getValidStates())
								models.put(state, mrl);
							return models;
						});
		});
	}
	
	@SubscribeEvent
	public void reloadColors(ReloadMetalColorsEvent e)
	{
		Object2IntMap<String> colors = e.colors;
		
		HMLog.LOG.info("Loading custom metal colors...");
		for(File f : HammerMetals.customMetalsDir.listFiles(file -> file.isFile() && file.getName().endsWith(".json") && !file.getName().startsWith("_")))
			try(BufferedReader br = Files.newBufferedReader(f.toPath()))
			{
				StringBuilder data = new StringBuilder();
				br.lines().forEach(data::append);
				JSONObject jo = (JSONObject) new JSONTokener(data.toString()).nextValue();
				jo.put("id", f.getName().substring(0, f.getName().length() - 5));
				if(jo.has("color"))
					colors.put(jo.optString("id"), jo.optInt("color"));
			} catch(Throwable err)
			{
				throw new ReportedException(CrashReport.makeCrashReport(err, "Exception caught while loading metals from json " + f.getAbsolutePath()));
			}
		
		for(ModContainer mod : Loader.instance().getActiveModList())
			HammerMetals.forEachHMetalFile(mod, (name, json) ->
			{
				if(json.has("color") && json.has("id"))
					colors.put(json.optString("id"), json.optInt("color"));
			});
	}
	
	@SubscribeEvent
	public void textureStitch(TextureStitchEvent.Pre e)
	{
		ReloadMetalColorsEvent rmce;
		MinecraftForge.EVENT_BUS.post(rmce = new ReloadMetalColorsEvent());
		for(String metal : rmce.colors.keySet())
		{
			MetalData md = MetalRegistry.getMetal(metal);
			if(md == null)
				continue;
			MetalInfo mi = md.info;
			mi.color = rmce.colors.getInt(metal);
		}
	}
	
	@Override
	public String getMaterial(MetalInfo info)
	{
		return info.useGeneratedLang ? info.langs.getLocalization("material") : super.getMaterial(info);
	}
}